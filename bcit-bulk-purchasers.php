<?php
/*
Plugin Name: Bulk Purchasers
Plugin URI:
Description: Adds bulk purchasers to our site and gives them super powers
Version: 1.0
Author: SFNdesign, Curtis McHale
Author URI: http://sfndesign.ca
License: GPLv2 or later
*/

/*
This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

require_once( 'meta.php' );

class Bcit_Bulk_Purchasers{

	function __construct(){

		add_action( 'admin_notices', array( $this, 'check_required_plugins' ) );

		add_action( 'pre_get_posts', array( $this, 'limit_our_posts' ) );

		// Register hooks that are fired when the plugin is activated, deactivated, and uninstalled, respectively.
		register_activation_hook( __FILE__, array( $this, 'activate' ) );
		register_deactivation_hook( __FILE__, array( $this, 'deactivate' ) );
		register_uninstall_hook( __FILE__, array( __CLASS__, 'uninstall' ) );

	} // construct

	/**
	 * Changes our query so that we don't show our posts where we don't want them
	 *
	 * @since 1.0
	 * @author SFNdesign, Curtis McHale
	 *
	 * @param array     $query$         required        The mayn WP_Query object on the page
	 * @uses is_admin()                                 Returns true if we are in the WordPress admin
	 * @uses is_main_query()                            Returns true if we are in the main query
	 * @uses $this->remove_posts_restricted()           Removes any posts that are restricted
	 * @uses is_user_logged_in()                        Returns true if user is logged in
	 */
	public function limit_our_posts( $query ){

		if ( is_admin() ) return;
		if ( ! $query->is_main_query() ) return;
		if ( $query->query_vars['post_type'] !== 'product' ) return;

		$this->remove_posts_restricted( $query );

		if ( is_user_logged_in() ) {
			add_filter( 'posts_where', array( $this, 'custom_where' ) );
		}

	} // limit_our_posts

	/**
	 * Make sure that if a user is logged in we include posts that have their user_id assigned in addition to the
	 * posts that have no key assigned.
	 *
	 * @since 1.0
	 * @author SFNdesign, Curtis McHale
	 *
	 * @param string        $where          required            existing SQL WHERE clauses in our query
	 * @global $wpdb                                            Biddy daddy WordPress dabatase global
	 * @uses get_current_user_id()                              Returns current user_id
	 * @return string       $where                              Our modified WP WHERE SQL clause
	 */
	public function custom_where( $where = '' ){

		global $wpdb;
		$user_id = get_current_user_id();

		$where .= " OR (( $wpdb->postmeta.meta_key = '_restricted_to' and $wpdb->postmeta.meta_value = $user_id ))";

		remove_filter( 'posts_where', array( $this, 'custom_where' ) );

		return $where;

	} // custom_where

	/**
	 * Removes any restricted posts if a user is not logged in.
	 *
	 * Note that NOT EXISTS requires a value of some sort due to an existing bug in WordPress.
	 *
	 * @since 1.0
	 * @author SFNdesign, Curtis McHale
	 *
	 * @param array         $query          required        wp_query array
	 * @param $query->set()                                 WP method to set existing query args while it's in WP_Query
	 */
	private function remove_posts_restricted( $query ){

		$query->set( 'meta_query', array(
				array(
					'key' => '_restricted_to',
					'value' => 'osnteuh',
					'compare' => 'NOT EXISTS',
				)
			)
		);

	} // remove_posts_restricted

	/**
	 * Checks for WooCommerce and GF and kills our plugin if they aren't both active
	 *
	 * @uses    function_exists     Checks for the function given string
	 * @uses    deactivate_plugins  Deactivates plugins given string or array of plugins
	 *
	 * @action  admin_notices       Provides WordPress admin notices
	 *
	 * @since   1.0
	 * @author  SFNdesign, Curtis McHale
	 */
	public function check_required_plugins(){

		if( ! is_plugin_active( 'woocommerce/woocommerce.php' ) ){ ?>

			<div id="message" class="error">
				<p>Bulk Purchasers expects WooCommerce to be active. This plugin has been deactivated.</p>
			</div>

			<?php
			deactivate_plugins( '/bcit-bulk-purchasers/bcit-bulk-purchasers.php' );
		} // compmany team if

	} // check_required_plugins

	/**
	 * Adds our custom role for users
	 *
	 * @since 1.0
	 * @author SFNdesign, Curtis McHale
	 *
	 * @uses add_role()                 Adds a role to the WP database
	 */
	public function add_custom_role(){
		add_role( 'bulkpurchasers', 'Corporate Bulk Purchasers' );
	} // add_custom_role

	/**
	 * Removes our custom role for users
	 *
	 * @since 1.0
	 * @author SFNdesign, Curtis McHale
	 *
	 * @uses remove_role()                 Adds a role to the WP database
	 */
	public function remove_custom_role(){
		remove_role( 'bulkpurchasers' );
	} // add_custom_role

	/**
	 * Fired when plugin is activated
	 *
	 * @param   bool    $network_wide   TRUE if WPMU 'super admin' uses Network Activate option
	 */
	public function activate( $network_wide ){

		$this->add_custom_role();

	} // activate

	/**
	 * Fired when plugin is deactivated
	 *
	 * @param   bool    $network_wide   TRUE if WPMU 'super admin' uses Network Activate option
	 */
	public function deactivate( $network_wide ){

	} // deactivate

	/**
	 * Fired when plugin is uninstalled
	 *
	 * @param   bool    $network_wide   TRUE if WPMU 'super admin' uses Network Activate option
	 */
	public function uninstall( $network_wide ){

		$this->remove_custom_role();

	} // uninstall

} // Bcit_Bulk_Purchasers

new Bcit_Bulk_Purchasers();
