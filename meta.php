<?php

/**
 * Does all the showing/saving of the custom meta items for the members CPT
 * in the WP admin area.
 *
 * @since 1.0
 * @author SFNdesign, Curtis McHale
 */
class Career_Custom_Meta{

	function __construct(){

		add_action( 'load-post.php', array( $this, 'metaboxes_setup' ) );
		add_action( 'load-post-new.php', array( $this, 'metaboxes_setup' ) );

	} // __construct

	/**
	 * Adds our actions so that we can start the build out of the post metaboxes
	 *
	 * @since   1.0
	 * @author  SFNdesign, Curtis McHale
	 */
	public function metaboxes_setup(){

		// adds the action which actually adds the meta boxes
		add_action( 'add_meta_boxes', array( $this, 'add_post_metaboxes' ) );

		// all the saving actions go here
		add_action( 'save_post', array( $this, 'save_post_meta' ), 10, 2 );

	} // metaboxes_setup

	/**
	 * Adds the actual metabox
	 *
	 * @uses    add_meta_box
	 *
	 * @since   1.0
	 * @author  SFNdesign, Curtis McHale
	 */
	public function add_post_metaboxes(){

		add_meta_box(
			'restricted-product',                // $id - HTML 'id' attribute of the edit screen section
			'Restrict Product To',               // $title - Title that will show at the top of the metabox
			array( $this, 'display_metaboxes' ), // $callback - The function that will display the metaboxes
			'product',                           // $posttype - The registered name of the post type you want to show on
			'side',                              // $context - Where it shows on the page. Possibilities are 'normal', 'advanced', 'side'
			'high'                               // $priority - How high should this display?
			//'$callback_args'                    // any extra params that the callback should get. It will already get the $post_object
		);

	} // add_post_meta

	/**
	 * Builds out the photo by metabox
	 *
	 * @since   1.0
	 * @author  SFNdesign, Curtis McHale
	 *
	 * @param   object  $object     req     The whole post object for the metabox
	 * @param   array   $box        rex     Array of the box arguements
	 * @uses wp_nonce_field()               Noncing is safe
	 * @uses get_post_meta()                Returns post_meta given post_id and key
	 * @uses $this->show_list_of_users()    echos select box of users
	 */
	public function display_metaboxes( $post_object, $box ){

		wp_nonce_field( basename( __FILE__ ), 'career_meta_nonce' );

		$limited_user_id = get_post_meta( absint( $post_object->ID ), '_restricted_to', true );
		$limited_to = isset( $limited_user_id ) ? absint( $limited_user_id ) : null;

	?>

		<p>
			<label for="user-list">User List</label>
			<?php $this->show_list_of_users( $limited_to ); ?><br />
			<span class="description">Our list of possible users</span>
		</p>

	<?php
	} // display_metaboxes

	/**
	 * Echos a list of users in to a select box
	 *
	 * @since 1.0
	 * @author SFNdesign, Curtis McHale
	 *
	 * @param int       $limited_to         required            User_id of our currently selected user if we have it
	 * @uses get_users()                                        Returns array of users given args
	 * @uses selected()                                         Fancy WordPress function to show the selected HTML
	 * @uses esc_attr()                                         Escaping is safe
	 */
	private function show_list_of_users( $limited_to ){

		$user_args = array(
			'role' => 'bulkpurchasers'
		);

		$custom_users = get_users( $user_args );

		$html = '<select name="user-list" id="user-list">';
			$html .= '<option value="0">Not Limited</option>';
			foreach( $custom_users as $u ){
				$html .= '<option '. selected( $limited_to, $u->ID, false ) .' value="'. absint( $u->ID ) .'">'. esc_attr( $u->display_name ) .'</option>';
			}

		$html .= '</select>';

		echo $html;

	} // show_list_of_users

	/**
	 * Saves the metaboxes
	 *
	 * @param   int     $post_id    req     The ID of the post we're saving8
	 * @param   object  $post       req     The whole post object
	 * @return  mixed
	 *
	 * @uses    wp_verify_nonce
	 * @uses    get_post_type_object
	 * @uses    delete_post_meta
	 * @uses    update_post_meta
	 *
	 * @since   1.0
	 * @author  SFNdesign, Curtis McHale
	 */
	public function save_post_meta( $post_id, $post ){

		// check the nonce before we do any processing
		if( !isset( $_POST[ 'career_meta_nonce' ] ) || !wp_verify_nonce( $_POST[ 'career_meta_nonce' ], basename( __FILE__ ) ) ) return $post_id;

		// get the post type object
		$post_type = get_post_type_object( $post->post_type );

		// make sure current user can save
		if( !current_user_can( $post_type->cap->edit_post, $post_id ) ) return $post_id;

		$user_list = empty( $_POST['user-list'] ) ? delete_post_meta( $post_id, '_restricted_to' ) : update_post_meta( $post_id, '_restricted_to', esc_attr( $_POST['user-list'] ) );

	}

} // Career_Custom_Meta

new Career_Custom_Meta();
