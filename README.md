# User Limited Products

This plugin limits a product to the assigned user. Only that user can see the product. They can also see all other products in the store that are not limited to another user.
